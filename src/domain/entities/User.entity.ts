import mongoose from 'mongoose'
export const userEntity = () => {
  const userSchema = new mongoose.Schema(
    {
      name: String,
      age: Number,
      email: String
    }
  )
  return mongoose.models.Users || mongoose.model('Users', userSchema)
}
export const userEntity2 = () => {
  const userSchema = new mongoose.Schema(
    {
      gender: String,
      name: Object,
      location: Object,
      email: String,
      login: Object,
      dob: Object,
      registered: Object,
      phone: String,
      cell: String,
      id: Object,
      picture: Object,
      nat: String

    }
  )
  return mongoose.models.contacts || mongoose.model('contacts', userSchema)
}
