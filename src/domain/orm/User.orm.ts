import { userEntity } from '../entities/User.entity'
import { LogError } from '../../utils/logger'

// Crud

/**
 * Method to obtain all users from collection "Users" in Mongo Server
 */
export const getAllUsers = async (): Promise<any[] | undefined> => {
  try {
    const userModel = userEntity()
    // Search all users
    return await userModel.find({ isDelete: false })
  } catch (error) {
    LogError(`[ORM ERROR]: Getting all users: ${error}`)
  }
}

// TODO
// Get user by ID
export const getUserById = async (id: String) :Promise<any | undefined> => {
  try {
    const userModel = userEntity()
    // Search user by id
    return await userModel.findById(id)
  } catch (error) {
    LogError(`[ORM ERROR]: Getting user by id: ${error}`)
  }
}
// Get user by Email
// Delete user by ID
export const deleteUserById = async (id: String): Promise<any | undefined> => {
  try {
    const userModel = userEntity()
    await userModel.deleteOne({ _id: id })
  } catch (error) {
    LogError(`[ORM ERROR]: Deleting user by id: ${id}`)
  }
}
// Create new user
export const createUser = async (user: any): Promise<any> => {
  try {
    const userModel = userEntity()
    // Create new user
    return await userModel.create(user)
  } catch (error) {
    LogError(`[ORM ERROR]: Creting user ${error}`)
  }
}
// Update User
export const updateUserById = async (id: String, user: any): Promise <any | undefined> => {
  try {
    const userModel = userEntity()
    // Update user
    return await userModel.findByIdAndUpdate(id, user)
  } catch (error) {
    LogError(`[ORM ERROR]: Updating user ${error}`)
  }
}
