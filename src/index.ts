import dotenv from 'dotenv'
import server from './server'
import { LogError, LogSuccess } from './utils/logger'

// Configure the .env file
dotenv.config()

const port = process.env.PORT || 8000

// execute server
server.listen(port, () => {
  LogSuccess('[Server ON]: Running in http://localhost:8000/api ')
})

// Control server error
server.on('error', (error) => {
  LogError(`[Server Error]: ${error}`)
})
