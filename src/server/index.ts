import express, { Express, Request, Response } from 'express'
// Environment variable
import dotenv from 'dotenv'

// Swagger
import swaggerUi from 'swagger-ui-express'

// Security
import cors from 'cors'
import helmet from 'helmet'

// TODO HTTPS

import rootRouter from '../routes'
import mongoose from 'mongoose'

// Configuration the .env file
dotenv.config()

// Create express App
const server: Express = express()

// Swagger Config and route
server.use(
  '/docs',
  swaggerUi.serve,
  swaggerUi.setup(undefined, {
    swaggerOptions: {
      url: '/swagger.json',
      explorer: true
    }
  })
)

// Define Server to use '/api/' and use rootRouter from 'index.ts' in routes
// From this point on over http://localhost:8000/api/...
server.use(
  '/api/',
  rootRouter
)

// Status sever
server.use(express.static('public'))

// TODO Mongoose Conection
mongoose.connect('mongodb://localhost:27017/codeverification')

// Security Config
server.use(helmet())
server.use(cors())

// Content type Config
server.use(express.urlencoded({ extended: true, limit: '50mb' }))
server.use(express.json({ limit: '50mb' }))

// Redirection config
// http://localhost:8000 --> http://localhost:8000/api
server.get('/', (req: Request, res: Response) => {
  res.redirect('/api/')
})

export default server
