/**
 * Root Router
 * Redirections to Routers
 */

import express, { Request, Response } from 'express'
import helloRouter from './HelloRouter'
import { LogInfo } from '../utils/logger'
import goodbyeRouter from './GoodbyeRouter'
import { serve } from 'swagger-ui-express'
import userRouter from './UserRouter'

// Server instance
const server = express()

// Router instance
const rootRouter = express.Router()

// Activate for request to http://localhost:8000/api
rootRouter.get('/', (req: Request, res: Response)=>{
  LogInfo('Get: http://localhost:8000/api/')
  // send hello world
  res.send('Hello world')
})

// Redirectons to Routers and controllers
server.use('/', rootRouter) // http://localhost:8000/api
server.use('/hello', helloRouter) // http://localhost:8000/api/hello -> helloRouter
server.use('/goodbye', goodbyeRouter) // http://localhost:8000/api/goodbye
// Add more routes to the app
server.use('/users', userRouter) // http://localhost:8000/api/users --> userRouter

export default server
