/* eslint-disable indent */
import { BasicResponse } from '../controller/types'
import express, { Request, Response } from 'express'
import { HelloController } from '../controller/HelloController'
import { LogInfo } from '../utils/logger'

// Router from express
const helloRouter = express.Router()

// GET -> http://localhost:8000/api/hello?name=David/
helloRouter.route('/')
    .get(async (req: Request, res: Response) => {
        // Obtener a Query Param
        // eslint-disable-next-line prefer-const
        let name: any = req?.query?.name
        LogInfo(`Query Param: ${name}`)
        // Controller instance execute method
        const controller: HelloController = new HelloController()
        // Obtain response
        const response: BasicResponse = await controller.getMessage(name)
        // Send to the client the response
        return res.send(response)
    })

    // Export Hello Router
    export default helloRouter
