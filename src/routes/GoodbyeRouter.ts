import express, { Request, Response } from 'express'
import { GoodbyeController } from '../controller/GoodbyeController'
import { LogInfo } from '../utils/logger'

// Router from express
const goodbyeRouter = express.Router()

// GET -> http://localhost:8000/api/hello?name=David/
goodbyeRouter.route('/')
  .get(async (req: Request, res: Response) => {
    // Obtain a Query Param
    const name: any = req?.query?.name
    LogInfo(`Query Param: ${name}`)
    // Controller instance execute method
    const controller: GoodbyeController = new GoodbyeController()
    // Obtain response
    const response = await controller.getMessage(name)
    // Send to the client the response
    return res.send(response)
  })

// Export Hello Router
export default goodbyeRouter
