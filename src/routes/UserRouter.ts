/* eslint-disable indent */
import { LogInfo } from '../utils/logger'
import express, { Request, Response } from 'express'
import { UserController } from '../controller/UsersController'

// Router from express
const userRouter = express.Router()

// GET -> http://localhost:8000/api/users?id=625595c968142a922f24c6b5
userRouter.route('/')
    .get(async (req: Request, res: Response) => {
        // Obtener a Query Param by ID
        const id: any = req?.query?.id
        LogInfo(`Query Param: ${id}`)
        // Controller instance execute method
        const controller: UserController = new UserController()
        // Obtain response
        const response: any = await controller.getUser(id)
        // Send to the client the response
        return res.send(response)
    })
    // Delete
    .delete(async (req: Request, res: Response) => {
         // Obtener a Query Param by ID
        const id: any = req?.query?.id
        LogInfo(`Query Param: ${id}`)

        // Controller instance execute method
        const controller: UserController = new UserController()
        // Obtain response
        const response: any = await controller.deleteUser(id)
        return res.send(response)
    })
    .post(async (req: Request, res: Response) => {
        const name: any = req?.query?.name
        const age: any = req?.query?.age
        const email: any = req?.query?.email
        const user = {
            name: name || 'default',
            age: age || 0,
            email: email || 'no email'
        }
       // Controller instance execute method
       const controller: UserController = new UserController()
       // Obtain response
       const response: any = await controller.createUser(user)
       return res.send(response)
    })
    .put(
        async (req: Request, res: Response) => {
           // Obtener a Query Param by ID
        const id: any = req?.query?.id
        const name: any = req?.query?.name
        const age: any = req?.query?.age
        const email: any = req?.query?.email
        LogInfo(`Query Param: ${id}, ${name}, ${age}, ${email}`)

        // Controller instance execute method
        const controller: UserController = new UserController()

        // Obtain response

        const user = {
            name: name,
            age: age,
            email: email
        }
        const response: any = await controller.updateUserById(id, user)
        return res.send(response)
        }
    )

    // Export Hello Router
    export default userRouter
