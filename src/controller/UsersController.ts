import { Delete, Get, Post, Put, Query, Route, Tags } from 'tsoa'
import { IUserController } from './interfaces'
import { LogSuccess, LogWarning } from '../utils/logger'

// ORM - Users Collection
import { createUser, deleteUserById, getAllUsers, getUserById, updateUserById } from '../domain/orm/User.orm'

@Route('/api/users')
@Tags('UserController')
export class UserController implements IUserController {
  /**
   * Endpoint to retrieve the users inthe collection "Users" of DB
   * @Params {string} id Id of user or empty
   * @returns User/s information
   */
  @Get('/')
  public async getUser (@Query() id?: String): Promise<any> {
    let response: any = ''
    if (id) {
      LogSuccess(`[/api/users] Get User By Id ${id}`)

      response = await getUserById(id)
      return response
    } else {
      LogSuccess('[/api/users] Get All Users Request')
      response = await getAllUsers()
      return response
    }
  }

  /**
   * Endpoint to delete one user from DB
   * @Params {string} id Id of user to delete
   * @returns Message of user correctly deleted
   */
  @Delete('/')
  public async deleteUser (@Query() id?: String): Promise<any> {
    let response: any = ''
    if (id) {
      LogSuccess(`[/api/users] Deleting User By Id ${id}`)

      await deleteUserById(id)
        .then((r) => {
          response = `User with ID: ${id} deleted succesfully`
        })
      return response
    } else {
      LogWarning('[/api/users] Delete User Request Whithout ID')
      return {
        message: 'Please provide an ID to remove user from database'
      }
    }
  }

  @Post('/')
  public async createUser (user: any): Promise<any> {
    let response: any = ''
    await createUser(user).then((r) => {
      LogSuccess(`[/api/users] Attempting to create user ${user}`)

      response = {
        message: `User created: ${user.name}`
      }
      return response
    })
  }

  @Put('/')
  public async updateUserById (@Query() id: String, user: any): Promise<any> {
    let response: any = ''
    if (id) {
      LogSuccess(`[/api/users] Updating User By Id ${id}`)

      await updateUserById(id, user)
        .then((r) => {
          response = `User with ID: ${id} updated succesfully`
        })
      return response
    } else {
      LogWarning('[/api/users] Update User Request Whithout ID')
      return {
        message: 'Please provide an ID to update user from database'
      }
    }
  }
}
