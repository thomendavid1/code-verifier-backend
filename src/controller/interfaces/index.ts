import { BasicResponse } from '../types'

export interface IHelloController {
    getMessage(name?: string): Promise<BasicResponse>
}

export interface IGoodbyeController {
    getMessage(name?: string): Promise<BasicResponse>
}

export interface IUserController {
    // Read all users from db or get user by id
    getUser(id?: String): Promise<any>
    // Delete user from db by id
    deleteUser(id?: String): Promise<any>
    // Create user
    createUser(user: any): Promise<any>
    // Udate user
    updateUserById(id: string, user: any): Promise<any>
}
