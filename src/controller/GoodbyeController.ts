import { BasicDateResponse } from './types'
import { IGoodbyeController } from './interfaces'
import { LogSuccess } from '../utils/logger'

export class GoodbyeController implements IGoodbyeController {
  public async getMessage (name?: string): Promise<BasicDateResponse> {
    LogSuccess('[/api/goodbye] Get Request')
    const tiempoTranscurrido = Date.now()
    const actualDate = new Date(tiempoTranscurrido).toLocaleDateString()
    return {
      message: `Goodbye ${name || 'user'},`,
      date: `${actualDate}`
    }
  }
}
